from Repository import Repository
from Term import Terme
from Date import Date
from Article import  Article
import re


class Parser:

    fields = {'title:', 'description:', 'date:','link:','rss:','author:','iter:'}

    def __init__(self, id):
        self.title_str = []
        self.decription_str = []
        self.data_str = []
        self.rss_str = []
        self.iter_str = []
        self.author_str = []
        self.link_str = []
        self.file_id = id


    def reset_params(self):
        self.title_str.clear()
        self.decription_str.clear()
        self.data_str.clear()
        self.rss_str.clear()
        self.iter_str.clear()
        self.author_str.clear()
        self.link_str.clear()

    def parse(self, *files):
        for file in files:
            f = open(file.name,'r')
            field = ''
            file_terms = []
            Repository.add_file(file)
            f_line = 0
            for line in f:
                    f_line += 1
                    fieldTemp = re.findall(r'\w+:',line)
                    if fieldTemp.__len__() > 0 and self.filed_check(fieldTemp[0]):
                                    field = fieldTemp[0]
                                    text = re.split(r'\w:', line, maxsplit=1)
                                    text = text[1]
                                    self.field_index(field,text)
                                    text = re.findall(r'\w+',text)
                                    for i in text:

                                        if file_terms.__contains__(i):
                                            Repository.add_frequancy(i,f_line,file.name)
                                        elif(Repository.is_contains(i)
                                         and not file_terms.__contains__(i)):
                                            Repository.add_id(i, f_line, file.name, file.name)
                                            file_terms.append(i)

                                        else:
                                            t = Terme()
                                            t.set_params(f_line, file.name, i, file.name)
                                            Repository.add_item(t)
                                            file_terms.append(i)


                    else:
                            text = re.findall(r'\w+', line)
                            if text.__len__() > 0:
                                for i in text:
                                    if file_terms.__contains__(i):
                                        Repository.add_frequancy(i, f_line, file.name)
                                    elif (Repository.is_contains(i)
                                    and not file_terms.__contains__(i)):
                                        Repository.add_id(i, f_line, file.name, file.name)
                                        file_terms.append(i)
                                    else:
                                        t = Terme()
                                        t.set_params(f_line, file.name, i, file.name)
                                        Repository.add_item(t)
                                        file_terms.append(i)
                            else:
                                continue

            f.close()

    def filed_check(self, field):

        if self.fields.__contains__ (field):
          return True
        else:
          return False

    def date_comparer(self, date_line):
        d = Date(date_line)
        return d



    '''
    def print_list(self):
        for i in self.title_str:
            print("Text in title " + i)
        for i in self.decription_str:
            print("Text in description " + i)
        for i in self.data_str:
            print("Text in date " + i)
        for i in self.rss_str:
            print("Text in rss " + i)
        for i in self.author_str:
            print("Text in author " + i)
        for i in self.link_str:
            print("Text in link " + i)
        for i in self.iter_str:
            print("Text in iter " + i)
    '''


    def field_index(self,field,line):
        try:
            if( field == 'title:'):
                self.title_str.append(line)
            elif(field == 'description:'):
                self.decription_str.append(line)
            elif(field == 'date:'):
                date_str = re.findall(r'[^\n]\w+', line)
                date_str = ''.join(date_str)
                self.data_str.append(self.date_comparer(date_str))
            elif (field == 'rss:'):
                self.rss_str.append(line)
            elif (field == 'link:'):
                self.link_str.append(line)
            elif (field == 'author:'):
                self.author_str.append(line)
            elif (field == 'iter:'):
                self.iter_str.append(line)
                self.set_article()
                self.reset_params()
        except Exception:
            print("Wrong field. Check fields list")

    def set_article(self):
        article = Article(self.data_str[0], self.author_str[0], self.title_str[0], self.rss_str[0], self.iter_str[0])
        Repository.articles.append(article)
