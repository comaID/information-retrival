from TER_FILE import File
from Parser import Parser
from Repository import Repository
from Keyword import  Keyword
from  Searcher import  Searcher
from  Date import Date
f = File("files.csv")
s = File("new_file.csv")
f.write(" title:  Boston Dynamics Project \n"
        " description: New robot was created\n"
        "date:Mon 11 April 2017 03:45:54-157775\n"
        "rss: files.csv \n"
        "author:  Marc Raibert\n"
        "link: http://www.bostondynamics.com/ \n"
        "iter: 60 days\n"
        )
s.write(" title: Neuralink Project\n"
        " description: New company is "
        "developing ultra high bandwidth brain-machine interfaces to connect humans and computers.\n"
        "date:Wed 12 April  2014  03:45:54-157775 \n"
        "rss: new_files.csv \n"
        "author:  Elon Musk \n"
        "link: 	https://neuralink.com/ \n"
        "iter: 30 days\n")

p = Parser(f.file_id)
p.parse(f,s)

key = Keyword("Project")
a = Searcher()
d1 = Date("Mon 11 April  2013  03:45:54-157775")
d2 = Date("Mon 11 April 2017  03:45:54-157775")
#Repository.display_terms()
#a.global_search(None,None,"Project",None,d1.num_date,d2.num_date)
#a.global_search(None,None,None,d1.num_date)
a.global_search("Elon Musk")
#a.global_search(None,"Marc")
#a.global_search(None,None,"Boston")
#a.global_search(None,None,"Project")
