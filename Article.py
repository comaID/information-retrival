from  datetime import datetime
class Article:

    def __init__(self, date, author, title, rss, inter):
        self.date = date
        self.author = author
        self.title = title
        self.rss = rss
        self.iter = inter
    def print_article(self):
        print("--------------")
        print("Article title: %s" % self.title)
        print("Article date: %s" % str(self.date.str_date))
        print("Article author: %s" % self.author)
        print("Article rss: %s" % self.rss)
        print("Article interval: %s" % self.iter)
        print("--------------")