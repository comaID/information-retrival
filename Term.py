import ID_Messenger
from  datetime import  datetime

class Terme:

    def __init__(self):
        self.ID_array = {}
        self.terme_name = ""
        self.frequancy = 0
        self.file_name = []

    def set_params(self, id, type, terme_name, file_name):
        self.ID_array[ID_Messenger.ID_Dict(id)] = type
        self.terme_name = terme_name
        self.frequancy += 1
        self.file_name.append(file_name)

    def frq_increase(self):
        self.frequancy += 1

    def add_id(self, id, type, file_name):
        self.ID_array[ID_Messenger.ID_Dict(id)] = type
        self.frequancy += 1
        self.file_name.append(file_name)

    def print_term(self):
        print("################################")
        print("Terme name: %s, with frequancy %d" % (self.terme_name, self.frequancy))
        print("Linked with next fields:")
        for id in self.ID_array.keys():
            print(" File line:  %d ---- File name: %s" % (id.id, self.ID_array[id]))
        '''print("Next files contains this term: ")
        for i in self.file_name:
            print(i)
        '''