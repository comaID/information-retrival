from ID_Messenger import ID_Dict
import re

class Repository:
    terms = []
    files = []
    articles =[]
    key_indexer = 1
    @staticmethod
    def add_item(item):
        Repository.terms.append(item)

    @staticmethod
    def add_frequancy(name, id, type):
        for term in Repository.terms:
            if term.terme_name == name:
                term.frq_increase()
                for id_s in list(term.ID_array):
                    if id_s.id == id and term.ID_array[id_s] == type:
                        continue
                    else:
                        term.ID_array[ID_Dict(id)] = type
                        break
            else:
                continue

    @staticmethod
    def add_id( name, id, type, file_name):
        for term in Repository.terms:
            if term.terme_name ==  name:
                term.add_id(id,type,file_name)
            else:
                continue

    @staticmethod
    def add_file(file):
        Repository.files.append(file)

    @staticmethod
    def display_terms():
        for i in Repository.terms:
            i.print_term()

    @staticmethod
    def display_article():
        for i in Repository.articles:
            i.print_article()
    @staticmethod
    def display_files():
        for i in Repository.files:
            print(i.file_id)
    @staticmethod
    def is_contains(name):
        for term in Repository.terms:
            if term.terme_name == name:
                return True
            else:
                continue
    @staticmethod
    def search_word(key_word):
        for term in Repository.terms:
            if term.terme_name == key_word:
                term.print_term()
            else:
                continue
    @staticmethod
    def term_filter(names, len, terme):
        for term in Repository.terms:
            for name in names:
                if term.terme_name == name:
                    len -= 1
                    terme.append(term)
                    if len == 0:
                        return  True
                else:
                    continue
        if len > 0:
            terme.clear()
            return False

    @staticmethod
    def k_wrd(key_line):
        full_line = key_line
        key_line = re.findall(r'\w+', key_line)
        if (key_line.__len__() > 1):
            terms = []
            key_lenght = key_line.__len__()
            if Repository.term_filter(key_line,key_lenght,terms):
                Repository.compare_terms(terms,key_line.__len__(),full_line)

            else:
                print("Not found")
        else:
            Repository.search_word(key_line[0])

    @staticmethod
    def compare_terms(terms,key_lenght,full_line):
        for key,value, in terms[0].ID_array.items():
            if Repository.comapre_items(key.id, value, terms[Repository.key_indexer], key_lenght, full_line):
                Repository.comapre_items(key.id, value, terms[Repository.key_indexer], key_lenght, full_line)
            else:
                Repository.key_indexer = 1



    @staticmethod
    def comapre_items(key,value,terme,key_lenght,full_line):
        for this_key,this_value in terme.ID_array.items():
            if this_key.id == key and this_value == value:
                Repository.key_indexer += 1
                if Repository.key_indexer == key_lenght:
                    Repository.search_in_line(this_key.id,this_value,full_line)
                    Repository.key_indexer = 1
                    continue
                return True
            else:
                continue
    @staticmethod
    def search_in_line(numb_line,file_name,full_string):
        f = open(file_name,'r')
        line = f.readlines()
        f_line = line[numb_line - 1]
        if f_line.__contains__(full_string):
            print("-------------")
            print("Found keywords:")
            print("File line: " + str(numb_line) + " ---- File name: " + file_name)
            print()

        else:
            print("Line not found")
        f.close()




