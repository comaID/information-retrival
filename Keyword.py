import re


class Keyword:
    rangeLine = ""
    begin_interval = 0
    end_interval = 0

    @staticmethod
    def __init__(range_in):
        Keyword.rangeLine = range_in

    @staticmethod
    def interval(begin_from=0, end_to=0):
        Keyword.begin_interval = begin_from
        Keyword.end_interval = end_to
