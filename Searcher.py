from Repository import Repository
import re


class Searcher:
    author = ""
    key_line = ""
    date = ""
    result_artilces = []

    def search_kwrds(self, key_line):
        full_line = key_line
        key_line = re.findall(r'\w+', key_line)
        if (key_line.__len__() > 1):
            terms = []
            key_lenght = key_line.__len__()
            if Repository.term_filter(key_line, key_lenght, terms):
                Repository.compare_terms(terms, key_line.__len__(), full_line)
            else:
                print("Not found")
        else:
            Repository.search_word(key_line[0])
    '''
    def search_by_date(self,date = None,_to = None,_from = None,result_array = None):
        try:
            if _to != None and _from != None:
                if result_array != None:
                    temp_array = result_array

                    for dates in temp_array:
                        if dates.date.num_date < _to and dates.date.num_date > _from:
                            temp_array.append(dates)
                    if temp_array.__len__() > 0:
                        result_array.clear()
                        return temp_array
                    else:
                        print("Not found")
                else:
                    for dates in Repository.articles:
                        if dates.date.num_date < _to and dates.date.num_date > _from:
                            self.result_artilces.append(dates)
            else:
                if result_array != None:
                    temp_array = result_array
                    for dates in temp_array:
                        if dates.date.num_date == date:
                            temp_array.append(dates)
                            return temp_array

                    if temp_array.__len__() > 0:
                        result_array.clear()
                        return temp_array
                    else:
                        print("Not found")
                else:
                    for dates in Repository.articles:
                        if dates.date.num_date == date:
                            self.result_artilces.append(dates)
        except Exception:
            print( "Wrong date in search field")
    '''
    def search_by_author(self,author, title = None):
        for authors in Repository.articles:
            if authors.author.__contains__(author):
                if title is not None:
                    if authors.title.__contains__(title):

                        self.result_artilces.append(authors)
                else:
                    self.result_artilces.append(authors)

    def search_by_title(self,title,author = None ):
        for titles in Repository.articles:
            if titles.title.__contains__(title):
                if author!= None:
                    if titles.author.__contains__(author):
                        self.result_artilces.append(titles)
                else:
                    self.result_artilces.append(titles)

    def global_search(self,keyword = None,author = None,title = None, date = None, from_date = None, to_date =None):

        if keyword is None:
            if author is not None and title is not None:
               self.search_by_author(author,title)
               if date is not None:
                   for dates in self.result_artilces:
                       if date == dates.date.num_date:
                           dates.print_article()
               elif from_date is not None and to_date is not None:
                   for dates in self.result_artilces:
                       if (dates.date.num_date >= from_date
                           and dates.date.num_date <= to_date):
                           dates.print_article()
                           continue
               else:
                   self.print_array()
            elif author is not None and title is None:
                self.search_by_author(author)
                if date is not None:
                    for  dates in self.result_artilces:
                        if date == dates.date.num_date:
                            dates.print_article()
                elif from_date is not None and to_date is not None:
                    for dates in self.result_artilces:
                        if (dates.date.num_date >= from_date
                            and dates.date.num_date <= to_date):
                            dates.print_article()
                            continue
                else:
                    self.print_array()
            elif title is not None and author is None:
                self.search_by_title(title)
                if date is not None:
                    for  dates in self.result_artilces:
                        if date == dates.date.num_date:
                            dates.print_article()
                elif from_date is not None and to_date is not None:
                    for dates in self.result_artilces:
                        if (dates.date.num_date >= from_date
                            and dates.date.num_date <= to_date):
                            dates.print_article()
                            continue
                else:
                    self.print_array()
            elif title is None and author is None:
                if date is not None:
                    for  dates in Repository.articles:
                        if date == dates.date.num_date:
                            dates.print_article()
                elif from_date is not None and to_date is not None:
                    for dates in Repository.articles:
                        if (dates.date.num_date >= from_date
                            and dates.date.num_date <= to_date):
                            dates.print_article()
                            continue
        else:
            self.search_kwrds(keyword)

    def print_array(self):
        for i in self.result_artilces:
            i.print_article()
